
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.layers import LSTM, Dropout
from tensorflow.keras.layers import TimeDistributed
#from tensorflow.keras.layers.core import Dense, Activation, Dropout, RepeatVector
from tensorflow.keras.optimizers import RMSprop, Adagrad, Adam, Adamax, Adadelta
#import matplotlib.pyplot as plt
import re
import pickle
import sys
import heapq
import time


path1 = 'data/sender1.txt'
text1 = open(path1).read().lower()

path2 = 'data/sender2.txt'
text2 = open(path2).read().lower()

path3 = 'data/sender3.txt'
text3 = open(path3).read().lower()

path4 = 'data/sender4.txt'
text4 = open(path4).read().lower()

path5 = 'data/sender5.txt'
text5 = open(path5).read().lower()

print("sender1 =", len(text1))
print("sender2 =", len(text2))
print("sender3 =",  len(text3))
print("sender4 =", len(text4))
print("sender5 =", len(text5))

def tokenize_text(text):
    
    REGEX_NON_ACSII = re.compile('[^\x00-\x7f]')
    text = re.sub(REGEX_NON_ACSII, "", text)
    
    return text

def preprocess_train(messages):
    tokens = tokenize_text(messages)
    messages = list(tokens)
    # train , test= createData(messages)
   
    path = 'data/sender1.txt'
    train_data = open(path).read()
    char_train = sorted(list(set(train_data)))
    
    # Creating a mapping from unique characters to indices
    char_indices = dict((c, i) for i, c in enumerate(char_train))
    indices_char = dict((i, c) for i, c in enumerate(char_train))

    return train_data, char_train, char_indices, indices_char


train_data, char_train, char_indices, indices_char = preprocess_train(text1)

print 'unique chars: ',  len(char_train)




