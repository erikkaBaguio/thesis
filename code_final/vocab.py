from spellchecker import SpellChecker

def unique(list1): 
  
    # intilize a null list 
    unique_list = [] 
      
    # traverse for all elements 
    for x in list1: 
        # check if exists in unique_list or not 
        if x not in unique_list: 
            unique_list.append(x) 
    # print list 
    return unique_list
    # for x in unique_list: 
    #     print x, 

def data2list(data_list):
    words = []
    for word in data_list:
        words.append(word)
    return words

spell = SpellChecker()
f = open('textData/train_data3.txt').read().split()
t = open('textData/test_data3.txt').read().split()
w = open("vocab/sender3.txt","w+")

words = data2list(f)
words_test = data2list(t)

uniq_train = unique(words)
uniq_test = unique(words_test)


misspelled = spell.unknown(uniq_train)
misspelled_all_train = 0

misspelled_test = 0

for x in uniq_train:
	count = 0
	for i in f:
		if(x == i):
			count = count + 1
	if(x in misspelled):
		misspelled_all_train = misspelled_all_train + count
	w.write("{}, {} \n".format(x,count))

percentage = ((float(len(words)) - misspelled_all_train)/len(words))*100

for x in uniq_test:
    count = 0
    for i in t:
        if(x == i):
            count = count + 1
    if(x in misspelled):
        misspelled_test = misspelled_test + count

percentage_test = ((float(len(words_test)) - misspelled_test)/len(words_test))*100

exist = 0

for data in uniq_test:
    if(data in uniq_train):
        exist = exist + 1



w.write("\n\n TRAINING \n\n ")

w.write("total_words_training, {} \n ".format(len(words)))
w.write("unique_words_training, {} \n".format(len(uniq_train)))
w.write("misspelled_training, {} \n ".format(len(misspelled)))
w.write("complete_english_words_training, {} \n ".format(percentage))
w.write("misspelled_all_train, {} \n ".format(misspelled_all_train))

w.write("\n\n TESTING \n\n ")
w.write("exist_words_in_test, {} \n".format(exist))
w.write("total_words_testing, {} \n \n".format(len(words_test)))
w.write("unique_words_test, {} \n \n".format(len(uniq_test)))
w.write("misspelled_all_test, {} \n \n".format(misspelled_test))
w.write("complete_english_words_testing, {} \n \n".format(percentage_test))

w.close()