import numpy as np
np.random.seed(42)
import tensorflow as tf
tf.set_random_seed(42)
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.layers import LSTM, Dropout
from tensorflow.keras.layers import TimeDistributed
#from tensorflow.keras.layers.core import Dense, Activation, Dropout, RepeatVector
from tensorflow.keras.optimizers import RMSprop
#import matplotlib.pyplot as plt
import pickle
import sys
import heapq
import time

start_time = time.time()
def elapsed(sec):
    if sec<60:
        return str(sec) + " sec"
    elif sec<(60*60):
        return str(sec/60) + " min"
    else:
        return str(sec/(60*60)) + " hr"

                                       

path = 'textData/train_data5.txt'
text = open(path).read().lower()

path2 = 'textData/test_data5.txt'
test_data = open(path2).read().lower()

chars = sorted(list(set(text)))
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

SEQUENCE_LENGTH = 40
model = load_model('vocab/keras_model_sender_5.h5')
history = pickle.load(open("vocab/history_sender_5.p", "rb"))


def prepare_input(text):
    x = np.zeros((1, SEQUENCE_LENGTH, len(chars)))
    for t, char in enumerate(text):
        x[0, t, char_indices[char]] = 1.
        
    return x

def sample(preds, top_n=3):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds)
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    return heapq.nlargest(top_n, range(len(preds)), preds.take)


def predict_completion(text):
    original_text = text
    generated = text
    completion = ''
    while True:
        x = prepare_input(text)
        preds = model.predict(x, verbose=0)[0]
        next_index = sample(preds, top_n=1)[0]
        next_char = indices_char[next_index]
        text = text[1:] + next_char
        completion += next_char
        if len(original_text + completion) > len(original_text) :
            return completion.strip()


def predict_completions(text, n=3):
    x = prepare_input(text)
    print(text)
    preds = model.predict(x, verbose=0)[0]
    next_indices = sample(preds, n)
    return [text.strip()+indices_char[idx] + predict_completion(text[1:] + indices_char[idx]) for idx in next_indices]


quotes = [
    "It is not a lack of love, but a lack of friendship that makes unhappy marriages.",
    "That which does not kill us makes us stronger.",
    "I'm not upset that you lied to me, I'm upset that from now on I can't believe you.",
    "And those who were seen dancing were thought to be insane by those who could not hear the music.",
    "It is hard enough to remember my opinions, without also remembering my reasons for them!"
]

# print(prepare_input("This is an example of input for our LSTM".lower()))

# print(predict_completions("it", 5))

test = test_data.split()
words_entered = len(test)

# count = 0
# count_all = 0
# for q in test_data:
#     seq = q.lower().split()
#     for s in seq:
#         target = s+' '
#         text = ''
#         for i in s:
#             word = text.rjust(40,' ')
#             print(text)
#             print(predict_completions(word, 5))
#             text = text+i
            # predict = predict_completions(word, 5)
            # if(target in predict):
            #     count_all = count_all + 1
            #     text = text+i
            # else :
            #     count = count + 1
            #     count_all = count_all + 1
            #     text = text+i
count_until_pred = 0
count_all = 0
hit_rate = 0

f= open("vocab/sender5_test.txt","w+")
f.write("char ,                         predictions,               count_until_pred,           char_count \n\n")

for q in test:
    target = q+" "
    r_target = q
    text = ""
    for i in target:
        word = text.rjust(40,' ')
        predictions = predict_completions(word, 5)
        print(predictions)
        if(r_target in predictions):
            count_all = count_all + 1
            f.write("{}, {}, {}, {}, {} \n".format(text,r_target, predictions,count_until_pred+1,count_all))
            text = text+i
        else :
            count_until_pred = count_until_pred + 1
            count_all = count_all + 1
            f.write("{} , {}, {}, {}, {} \n".format(text,r_target, predictions,count_until_pred,count_all))
            text = text+i

f.write("--- %s seconds -- \n" % elapsed(time.time() - start_time))
f.write("count_until_pred : {} \n".format(count_until_pred))
f.write("all characters : {} \n".format(count_all))
f.write("number of words : {} \n".format(words_entered))
f.write("KSS : {} \n".format((float(count_all - count_until_pred) / count_all) *100))
f.write("KuP : {} \n".format(float(count_until_pred/ words_entered)))
f.close()
print("--- %s seconds ---" % elapsed(time.time() - start_time))

