import numpy as np
np.random.seed(42)
import tensorflow as tf
tf.set_random_seed(42)
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.layers import LSTM, Dropout
from tensorflow.keras.layers import TimeDistributed
#from tensorflow.keras.layers.core import Dense, Activation, Dropout, RepeatVector
from tensorflow.keras.optimizers import RMSprop, Adagrad, Adam, Adamax, Adadelta
#import matplotlib.pyplot as plt
import re
import pickle
import sys
import heapq
import time


start_time = time.time()
def elapsed(sec):
    if sec<60:
        return str(sec) + " sec"
    elif sec<(60*60):
        return str(sec/60) + " min"
    else:
        return str(sec/(60*60)) + " hr"
        
def train_val_test_split(messages, split_frac):

    """
    Zero Pad input messages
    :param messages: Input list of encoded messages
    :param labels: Input list of encoded labels
    :param split_frac: Input float, training split percentage
    :return: tuple of arrays train_x, val_x, test_x, train_y, val_y, test_y
    """
    # make sure that number of messages and labels allign
    # assert len(messages) == len(labels)
    # random shuffle data
    
    shuf_idx = len(messages)
    messages_shuf = np.array(messages)
    # labels_shuf = np.array(labels)[shuf_idx]

    #make splits
    split_idx = int(len(messages_shuf)*split_frac)
    train_x, test_x = messages_shuf[:split_idx], messages_shuf[split_idx:]

    return train_x, test_x      


def tokenize_text(text):
    
    REGEX_NON_ACSII = re.compile('[^\x00-\x7f]')
    text = re.sub(REGEX_NON_ACSII, "", text)
    
    return text

# def createData(messages):
#     x = []
#     y = []
#     x, y = train_val_test_split(messages, split_frac = 0.80)
#     train_data = open("textData/train_data5.txt", "w")
#     test_data =  open("textData/test_data5.txt", "w")



def preprocess_train(messages):
    tokens = tokenize_text(messages)
    messages = list(tokens)
    # train , test= createData(messages)
   
    path = 'textData/train_data5.txt'
    train_data = open(path).read()
    char_train = sorted(list(set(train_data)))
    
    # Creating a mapping from unique characters to indices
    char_indices = dict((c, i) for i, c in enumerate(char_train))
    indices_char = dict((i, c) for i, c in enumerate(char_train))

    return train_data, char_train, char_indices, indices_char


path = 'textData/train_data5.txt'
text = open(path).read().lower()

print 'corpus length:', len(text)

train_data, char_train, char_indices, indices_char = preprocess_train(text)

print 'unique chars: ',  len(char_train)

SEQUENCE_LENGTH = 40
step = 3
sentences = []
next_chars = []
for i in range(0, len(train_data) - SEQUENCE_LENGTH, step):
    sentences.append(train_data[i: i + SEQUENCE_LENGTH])
    next_chars.append(train_data[i + SEQUENCE_LENGTH])
print 'num training examples: ',  len(sentences)


X = np.zeros((len(sentences), SEQUENCE_LENGTH, len(char_train)), dtype=np.bool)
y = np.zeros((len(sentences), len(char_train)), dtype=np.bool)
for i, sentence in enumerate(sentences):
    for t, char in enumerate(sentence):
        X[i, t, char_indices[char]] = 1
    y[i, char_indices[next_chars[i]]] = 1


model = Sequential()
model.add(LSTM(128, input_shape=(SEQUENCE_LENGTH, len(char_train))))
model.add(Dense(len(char_train)))
model.add(Activation('softmax'))



optimizer = Adagrad(lr=0.01)
model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
history = model.fit(X, y, validation_split=0.05, batch_size=258, epochs=50, shuffle=True).history


model.save('vocab/keras_model_sender_5.h5')
pickle.dump(history, open("vocab/history_sender_5.p", "wb"))

print("--- %s ---" % elapsed(time.time() - start_time))


