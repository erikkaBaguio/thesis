from processing import load_char_vocab
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import time
from lstm import load_test_data


init = tf.global_variables_initializer()

class Hyperparams:
    '''Hyper parameters'''
    batch_size = 64
    embed_dim = 300
    seqlen = 50 

def kss():
    with tf.Session() as sess:
        # Run the initializer
        sess.run(init)
        
        # restore parameters
        # new_saver = tf.train.import_meta_graph('data/model_trainData.meta')
        # new_saver.restore(sess, tf.train.latest_checkpoint('./data'))
        # print("Restored!")
        # trained_data = open('data/checkpoint', 'r').read().split('"')[1] # model name


        # load test data
        char2idx, idx2char = load_char_vocab()
        # print(char2idx)
        # print(idx2char)
        # print('\n\n')


        test_x, test_y, v = load_test_data()
        # print(v)
        # print(len(test_x))

        chars, targets = [], [] # targets: the word that the char composes
        for x in test_y:
            # print(v[x])
            stop_counting = False


            # chars.append(" ")
            targets.append(v[x])

            for char in v[x]:
                chars.append(char)
            # targets.append(v[x])
            
            # x = np.concatenate( (np.zeros((Hyperparams.seqlen-1,),), 
            #                      x[-np.count_nonzero(x):]))# lstrip and zero-pad\

            # para = "".join(v[x])
            # print(para)

        print(targets)


if __name__ == '__main__':
    kss()
    print('DONE!')