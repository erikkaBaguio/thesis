import re
import string
from bidict import bidict
import xmltodict
import numpy as np
from collections import Counter
from itertools import chain



messages = []

def read_data(fname):
    with open(fname) as f:
        content = f.readlines()
    return content

def train_val_test_split(messages, split_frac):
    """
    Zero Pad input messages
    :param messages: Input list of encoded messages
    :param labels: Input list of encoded labels
    :param split_frac: Input float, training split percentage
    :return: tuple of arrays train_x, val_x, test_x, train_y, val_y, test_y
    """
    # make sure that number of messages and labels allign
    # assert len(messages) == len(labels)
    # random shuffle data
    
    shuf_idx = len(messages)
    messages_shuf = np.array(messages)
    # labels_shuf = np.array(labels)[shuf_idx]

    #make splits
    split_idx = int(len(messages_shuf)*split_frac)
    train_x, test_x = messages_shuf[:split_idx], messages_shuf[split_idx:]

    return train_x, test_x      

def segment_text(text):
    # text = re.sub("\r\n", "\n", text)
    for t in text:
        segmented_text = filter(None, t.split("<BEGIN>"))

        return segmented_text


def tokenize_text(text):

    REGEX_PRICE_SIGN = re.compile(r'\$(?!\d*\.?\d+%)\d*\.?\d+|(?!\d*\.?\d+%)\d*\.?\d+\$')
    REGEX_PRICE_NOSIGN = re.compile(r"(?!\d*\.?\d+%)(?!\d*\.?\d+k)\d*\.?\d+")
    REGEX_TICKER = re.compile('\$[a-zA-Z]+')
    REGEX_USER = re.compile('\@\w+')
    REGEX_LINK = re.compile('https?:\/\/[^\s]+')
    REGEX_HTML_ENTITY = re.compile('\&\w+')
    REGEX_NON_ACSII = re.compile('[^\x00-\x7f]')
    REGEX_PUNCTUATION = re.compile('[%s]' % re.escape(string.punctuation))
    REGEX_NUMBER = re.compile(r'[-+]?[0-9]+')
    REGEX_EMAIL = re.compile(r'([w.-]+)@([w.-]+)')

    # Replace ST "entitites" with a unique token
    text = re.sub(REGEX_TICKER, ' <TICKER> ', text)
    text = re.sub(REGEX_USER, ' <USER> ', text)
    text = re.sub(REGEX_LINK, ' <LINK> ', text)
    text = re.sub(REGEX_PRICE_SIGN, ' <PRICE> ', text)
    text = re.sub(REGEX_PRICE_NOSIGN, ' <UNKNOWN> ', text)
    text = re.sub(REGEX_NUMBER, ' <NUMBER> ', text)
    text = re.sub(REGEX_EMAIL, ' <EMAIL> ', text)
    # Remove extraneous text data
    text = re.sub(REGEX_HTML_ENTITY, "", text)
    text = re.sub(REGEX_NON_ACSII, "", text)
    text = re.sub(REGEX_PUNCTUATION, "", text)
    text = re.sub(r"([.,!?])", r" \1 ", text)
    text = re.sub(r"\s{2,}", r" ", text)
    # text = re.sub(r"([w.-]+)@([w.-]+)")
    return text.split()


def generate_ngrams(tokens, ngram_size=3):

    # Add (n-1) Special tags/Start Padding
    for _ in xrange(ngram_size - 1):
        tokens.insert(0, "<__START__>")

    unique_tokens = set(tokens)
    ngrams = zip(*[tokens[i:] for i in range(ngram_size)])

    return unique_tokens, ngrams


def preprocess(messages, ngram_size=3):
    num_tokens = 0
    vocabulary, total_ngrams, sent = set(), [], []


    for message in messages :
        tokens = tokenize_text(message)
        sent.append(tokens)
        num_tokens += len(tokens)

        unique_tokens, ngrams = generate_ngrams(tokens, ngram_size)

        vocabulary = vocabulary.union(unique_tokens)
        voc = vocabulary
        total_ngrams.extend(ngrams)

    temp = " ".join(chain.from_iterable(sent))
    words = temp.split()

    vocabulary.add("<__UNK__>")

    names = ['id','data']
    formats = ['int','string']
    dtype = dict(names = names, formats=formats)


    # generate integer ID for each word

    dictionary1 = dict()
    for message in vocabulary:
        dictionary1[message] = len(dictionary1)
    
    # dictionary1 = dict({i: w for i, w in enumerate(vocabulary)})


    dictionary = bidict({i: w for i, w in enumerate(vocabulary)})

    reverse_vocabulary=dict(zip(dictionary1.values(),  dictionary1.keys()))
    # reverse_vocabulary=dict({i: w for i, w in dictionary.iteritems()})


    # represent each word into integer ID
    ngrams = [map(lambda x: dictionary.inv[x], i) for i in total_ngrams]
    

    return {
        "vocabulary": vocabulary,
        "dictionary": dictionary1,
        "reverse_vocabulary": reverse_vocabulary,
        "num_tokens": num_tokens,
        "ngrams": ngrams,
        "voc":voc, 
        "unique_tokens": unique_tokens,
        "words": words
        }

# Build Training data. For example if X = ['long', 'ago', ','] then Y = ['the']
def sampling(train_data2, train_dictionary, ngram_size):
    X = []
    Y = []
    sample = []
    for index in list(range(0, len(train_dictionary) - ngram_size)):
        for i in list(range(0, ngram_size)):
            sample.append(train_dictionary[train_data2[index + i]])
            if (i + 1) % ngram_size == 0:
                X.append(sample)
                Y.append(train_dictionary[train_data2[index + i +1]])
                sample = []
    return X,Y

def load_char_vocab():
    vocab = "EU abcdefghijklmnopqrstuvwxyz0123456789-.,?!'" # E: Empty, U:Unknown
    char2idx = {char:idx for idx, char in enumerate(vocab)}
    idx2char = {idx:char for idx, char in enumerate(vocab)}  
    
    return char2idx, idx2char

def main():
    training_file = 'data/sender1.txt'
    training_data = read_data(training_file)
    messages = segment_text(training_data)
    train_data1, test_data = train_val_test_split(messages, split_frac = 0.80)

    train_data = preprocess(train_data1,3)
    train_vocabulary = train_data['vocabulary'] # all words
    train_dictionary = train_data['dictionary'] # words and id
    train_reverse_dictionary = train_data['reverse_vocabulary']
    train_ngrams = train_data['ngrams']
    print(len(train_ngrams)) 
    words = train_data['words']
    
    train_words= list(words)
    ngram_size = 3
    train_x, train_y = sampling(train_words, train_dictionary, ngram_size)




if __name__ == '__main__':
    main()
