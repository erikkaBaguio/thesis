import xml.etree.ElementTree as ET

tree = ET.parse('smsCorpus_en_2015.03.09_all.xml')
root = tree.getroot()
with open('testdata.txt', 'w') as f:

    # senders = {}
    # sorted_senders = {}
    sender1 = '23249055a638bbc9b1fc5eb7dac9b4259524183451bc74bc'
    sender2 = '0'
    sender3 = '975bd06869dccbc3b74b999a840f68a537795b6c8b833dfe'
    sender4 = 'c3e3c793bbdb71d520fa190b5c23b4cf796b52d96b40d9d938cc3db68eb0e909'
    sender5 = 'df501e0b21bc88cc1e464dd95bf722da8865851c93267498'

    for message in root.findall('message'):
        src = message.find('source/srcNumber').text.encode('utf-8') 
        msg = message.find('text').text.encode('utf-8') 

        line = msg + '<BEGIN>'
        
        if sender1 == src:
            with open('data/sender1.txt', 'a') as f:
                f.write(line)

    	if sender2 == src:
            with open('data/sender2.txt', 'a') as f:
                f.write(line)

        if sender3 == src:
            with open('data/sender3.txt', 'a') as f:
                f.write(line)

        if sender4 == src:
            with open('data/sender4.txt', 'a') as f:
                f.write(line)

        if sender5 == src:            	        
            with open('data/sender5.txt', 'a') as f:
                f.write(line)


    	
    	# if src not in senders:
    	# 	senders[src] = 1
    	# else:
    	# 	senders[src] = senders.get(src) + 1



    # loop for identifying top 5 senders with highest number of messages
	# for key, value in sorted(senders.iteritems(), key=lambda(k,v): (v,k)):
	#     print "%s: %s" % (key, value)


    # print('\n\n\n')
    # print(sorted_senders)
    # print(len(sorted_senders))
    # print('\n\n\n')