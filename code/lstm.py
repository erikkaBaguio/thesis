import collections
import re, string
import os
import sys
from collections import Counter
from processing import preprocess, train_val_test_split, segment_text, read_data, sampling
import xmltodict
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import time


messages = []

def elapsed(sec):
    if sec<60:
        return str(sec) + " sec"
    elif sec<(60*60):
        return str(sec/60) + " min"
    else:
        return str(sec/(60*60)) + " hr"


logs_path = '/tmp/tensorflow/rnn_words'
writer = tf.summary.FileWriter(logs_path)

training_file = 'data/sender5.txt'
training_data = read_data(training_file)

messages = segment_text(training_data)

train_data1, test_data = train_val_test_split(messages, split_frac = 0.80)


def load_test_data():
    tst_data = preprocess(test_data, 3)
    ngram_size = 3
    words = tst_data['words']
    test_words = list(words)
    test_dictionary = tst_data['dictionary']
    test_reverse_dictionary = tst_data['reverse_vocabulary']
    test_x, test_y = sampling(test_words, test_dictionary, ngram_size)
    vocabulary = tst_data['vocabulary']

    return test_x, test_y, test_reverse_dictionary


train_data = preprocess(train_data1, 3)

train_vocabulary = train_data['vocabulary'] # all words
train_dictionary = train_data['dictionary'] # words and id
train_reverse_dictionary = train_data['reverse_vocabulary']
train_ngrams = train_data['ngrams'] 
# train_num_tokens = train_data['data']
train_voc = train_data['voc'] 
unique_tokens = train_data['unique_tokens']
words = train_data['words']

#get the max length of message
# messages_lens = Counter([len(x) for x in train_data ])
# print("Maximum message length: {}".format(max(messages_lens)))

#TRAINING

#Parameters
ngram_size = 3
dict_size = len(train_dictionary)
timesteps = ngram_size
num_hidden = 256
num_input = 1
batch_size = 64
iteration = 800

train_words= list(words)

train_x, train_y = sampling(train_words, train_dictionary, ngram_size)


# RNN output node weights and biases
weights = {
    'out': tf.Variable(tf.random_normal([num_hidden, dict_size]))
}
biases = {
    'out': tf.Variable(tf.random_normal([dict_size]))
}

# tf graph input
X = tf.placeholder("float", [None, timesteps, num_input])
Y = tf.placeholder("float", [None, dict_size])


def RNN(x, weights, biases):
    # reshape to [1, n_input]
    x = tf.reshape(x, [-1, timesteps])

    # Generate a n_input-element sequence of inputs
    # (eg. [had] [a] [general] -> [20] [6] [33])
    x = tf.split(x, timesteps, 1)

    # 2-layer LSTM, each layer has num_hidden units.
    # Average Accuracy= 95.20% at 50k iter
    lstm_cell = rnn.MultiRNNCell([rnn.BasicLSTMCell(num_hidden, forget_bias=1.0),rnn.BasicLSTMCell(num_hidden, forget_bias=1.0)])

    # 1-layer LSTM with num_hidden units but with lower accuracy.
    # Average Accuracy= 90.60% 50k iter
    # Uncomment line below to test but comment out the 2-layer rnn.MultiRNNCell above
    # rnn_cell = rnn.BasicLSTMCell(num_hidden)

    # generate prediction
    outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)

    # there are n_input outputs but
    # we only want the last output
    return tf.matmul(outputs[-1], weights['out']) + biases['out']


logits = RNN(X, weights, biases)
prediction = tf.nn.softmax(logits)

# Loss and optimizer
loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=Y))
train_op = tf.train.AdadeltaOptimizer(learning_rate=0.001).minimize(loss_op)
# train_op = AdamaxOptimizer(learning_rate=0.001).minimize(loss_op)
correct_pred = tf.equal(tf.argmax(prediction,1), tf.argmax(Y,1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

#Save the train data
saver = tf.train.Saver()

# Initialize the variables with default values
init = tf.global_variables_initializer()
start_time = time.time()

lta = 0
lfloss = 0
sfloss = 1000
s1 = 0
s2 = 0

with tf.Session() as sess:
    # Run the initializer
    sess.run(init)
    writer.add_graph(sess.graph)

    for i in range(iteration):
        last_batch = len(train_x) % batch_size
        training_steps = (len(train_x) / batch_size) + 1
        for step in range(training_steps):
            X_batch = train_x[(step * batch_size) :((step + 1) * batch_size)]
            Y_batch = train_y[(step * batch_size) :((step + 1) * batch_size)]
            Y_batch_encoded = []
            for x in Y_batch:
                on_hot_vector = np.zeros([dict_size], dtype=float)
                on_hot_vector[x] = 1.0
                Y_batch_encoded = np.concatenate((Y_batch_encoded,on_hot_vector))
            if len(X_batch) < batch_size:
                X_batch = np.array(X_batch)
                X_batch = X_batch.reshape(last_batch, timesteps, num_input)
                Y_batch_encoded = np.array(Y_batch_encoded)
                Y_batch_encoded = Y_batch_encoded.reshape(last_batch, dict_size)
            else:
                X_batch = np.array(X_batch)
                X_batch = X_batch.reshape(batch_size, timesteps, num_input)
                Y_batch_encoded = np.array(Y_batch_encoded)
                Y_batch_encoded = Y_batch_encoded.reshape(batch_size, dict_size)
            _, acc, loss, onehot_pred = sess.run([train_op, accuracy, loss_op, logits], feed_dict={X: X_batch, Y: Y_batch_encoded})
            saver.save(sess, "sender5/model_sender5_256_Adadelta_001")
            print("Step " + str(i) + ", Minibatch Loss= " + "{:.4f}".format(loss) + ", Training Accuracy= " + "{:.2f}".format(acc * 100))

            ta = acc*100

            
            if lta <= ta:
                lta = ta
                lfloss = loss
                s1= i
                
            if sfloss > loss:
                sfloss = loss
                s2 = i

    
    print('\n\n\n')
    print(lta)
    print(lfloss)
    print(s1)
    print('\n\n\n')
    print(sfloss)
    print(s2)
    print('\n\n\n')
    print("\nOptimization Finished!")
    print("Elapsed time: ", elapsed(time.time() - start_time))
