import re
import string
from bidict import bidict
import xmltodict
import numpy as np
from collections import Counter
from itertools import chain
import regex
from nltk.tokenize import sent_tokenize
import tensorflow as tf
# from tensorflow.contrib import rnn
import time

tf.enable_eager_execution()

messages = []

def read_data(fname):
    with open(fname) as f:
        content = f.readlines()
    return content


def tokenize_text(text):
    REGEX_PRICE_SIGN = re.compile(r'\$(?!\d*\.?\d+%)\d*\.?\d+|(?!\d*\.?\d+%)\d*\.?\d+\$')
    REGEX_PRICE_NOSIGN = re.compile(r"(?!\d*\.?\d+%)(?!\d*\.?\d+k)\d*\.?\d+")
    REGEX_TICKER = re.compile('\$[a-zA-Z]+')
    REGEX_USER = re.compile('\@\w+')
    REGEX_LINK = re.compile('https?:\/\/[^\s]+')
    REGEX_HTML_ENTITY = re.compile('\&\w+')
    REGEX_NON_ACSII = re.compile('[^\x00-\x7f]')
    REGEX_PUNCTUATION = re.compile('[%s]' % re.escape(string.punctuation))
    REGEX_NUMBER = re.compile(r'[-+]?[0-9]+')
    REGEX_EMAIL = re.compile(r'([w.-]+)@([w.-]+)')

    # # Replace ST "entitites" with a unique token
    text = re.sub(REGEX_TICKER, ' <TICKER> ', text)
    text = re.sub(REGEX_USER, ' <USER> ', text)
    text = re.sub(REGEX_LINK, ' <LINK> ', text)
    text = re.sub(REGEX_PRICE_SIGN, ' <PRICE> ', text)
    text = re.sub(REGEX_NUMBER, "", text)
    text = re.sub(REGEX_PRICE_NOSIGN, ' <UNKNOWN> ', text)  
    text = re.sub(REGEX_EMAIL, ' <EMAIL> ', text)
    # # Remove extraneous text data
    text = re.sub(REGEX_HTML_ENTITY, "", text)
    text = re.sub(REGEX_NON_ACSII, "", text)
    text = re.sub(REGEX_PUNCTUATION, "", text)
    # text = re.sub(r"([.,!?])", r" \1 ", text)
    # text = re.sub(r"\s{2,}", r" ", text)
    # text = re.sub(r"([w.-]+)@([w.-]+)")
    # return text.split()
    return text

def train_val_test_split(messages, split_frac):

    """
    Zero Pad input messages
    :param messages: Input list of encoded messages
    :param labels: Input list of encoded labels
    :param split_frac: Input float, training split percentage
    :return: tuple of arrays train_x, val_x, test_x, train_y, val_y, test_y
    """
    # make sure that number of messages and labels allign
    # assert len(messages) == len(labels)
    # random shuffle data
    
    shuf_idx = len(messages)
    messages_shuf = np.array(messages)
    # labels_shuf = np.array(labels)[shuf_idx]

    #make splits
    split_idx = int(len(messages_shuf)*split_frac)
    train_x, test_x = messages_shuf[:split_idx], messages_shuf[split_idx:]

    return train_x, test_x      


def preprocess(messages):
    tokens = tokenize_text(messages)
    messages = list(tokens)
    train_data, test_data = train_val_test_split(messages, split_frac = 0.80)

    vocab = sorted(set(tokens))
    
    # Creating a mapping from unique characters to indices
    char2idx = {u:i for i, u in enumerate(vocab)}
    idx2char = np.array(vocab)

    text_as_int = np.array([char2idx[c] for c in train_data])
   

    return  vocab, char2idx, idx2char, text_as_int, train_data, test_data


def main():
    
    training_file = 'data/sender5.txt'
    # training_data = read_data(training_file)
    training_data = open(training_file).read().decode(encoding='utf-8')
    # 237491

    vocab, char2idx, idx2char, text_as_int = preprocess(training_data)

    seq_length = 100
    examples_per_epoch = len(training_data)//seq_length
    # Create training examples / targets
    char_dataset = tf.data.Dataset.from_tensor_slices(text_as_int)
    # for i in char_dataset.take(5):
    #     print(idx2char[i.numpy()])

    sequences = char_dataset.batch(seq_length+1, drop_remainder=True)
    for item in sequences.take(5):
        print(repr(''.join(idx2char[item.numpy()])))


    # print('{')
    # for char,_ in zip(char2idx, range(20)):
    #     print('  {:4s}: {:3d},'.format(repr(char), char2idx[char]))
    # print('  ...\n}')

    # # Show how the first 13 characters from the text are mapped to integers
    # print ('{} ---- characters mapped to int ---- > {}'.format(repr(training_data[:13]), text_as_int[:13]))

if __name__ == '__main__':
    main()